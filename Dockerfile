FROM python:3.9-slim-buster

RUN apt-get update && apt-get install -y build-essential

COPY requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt

COPY . /app
WORKDIR /app
CMD ["python", "main.py"]
