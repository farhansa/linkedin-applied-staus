import requests
from bs4 import BeautifulSoup
import pandas as pd
from google.oauth2.service_account import Credentials
from googleapiclient.discovery import build
from telebot import TeleBot, types
from datetime import datetime
from telebot.types import InlineKeyboardMarkup
from dotenv import load_dotenv
import os
from urllib.parse import urlparse


load_dotenv()

TELEGRAM_TOKEN = os.environ['TELEGRAM_TOKEN']
SPREADSHEET_ID =os.environ['SPREADSHEET_ID']
SHEET_NAME = os.environ['SHEET_NAME']

# Initialize the Telegram bot
bot = TeleBot(TELEGRAM_TOKEN)

# Use the service account credentials to create a Credentials object
creds = Credentials.from_service_account_file("cred.json")

# Build the Sheets API client
service = build('sheets', 'v4', credentials=creds)

# Specify the spreadsheet and range of data to read
spreadsheet_id = SPREADSHEET_ID
range_ = f'{SHEET_NAME}!A1:Z'

# Use the Sheets API to read the data
def read_dataframe():
    global df
    # Use the Sheets API to read the data
    result = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id, range=range_).execute()
    values = result.get('values', [])
    # Convert the data to a Pandas DataFrame
    if values:
        df = pd.DataFrame(values[1:], columns=values[0])
    else:
        df = pd.DataFrame()
read_dataframe()

def get_linkedin_job_detail(url):
    try:
        # Make a GET request to the page
        page = requests.get(url)
        page.raise_for_status()
        # Create a BeautifulSoup object
        soup = BeautifulSoup(page.text, 'html.parser')

        # Extract the title of the page
        title = soup.title.get_text()
        title_parts = title.split(" ")

        # The first part is the title of the job
        company = title_parts[0]
        # print(job_title)

        job_title = title.split("hiring")[1].split(' in ')[0].strip()
        # print(company)

        location = title.split(" in ")[1].split(' |')[0].strip()
        # print(location)
        return {
            'job_title':job_title,
            'company':company,
            'location':location
        }
    except requests.exceptions.HTTPError as errh:
        print ("HTTP Error:",errh)
    except requests.exceptions.ConnectionError as errc:
        print ("Error Connecting:",errc)
    except requests.exceptions.Timeout as errt:
        print ("Timeout Error:",errt)
    except requests.exceptions.RequestException as err:
        print ("Something went wrong",err)
    except IndexError:
        print("Error: 'hiring' not found in title")

def job_exists(job_detail, df):
    if job_detail is None:
        return "The job details could not be obtained from the URL"
    else:
        job_title = job_detail['job_title']
        company = job_detail['company']
        match = df[(df['Role'] == job_title) & (df['Company'] == company)]
        if match.empty:
            return "does not exist"
        else:
            return "you applied this job before"

def write_to_sheet(job_detail, service, spreadsheet_id, range_,url):
    today = datetime.now().strftime("%d/%m/%Y")
    job_detail_list = [[today, job_detail['job_title'], job_detail['company'], '', '', job_detail['location'], url ]]
    # Use the Sheets API to append the data to the sheet
    result = service.spreadsheets().values().append(spreadsheetId=spreadsheet_id, range=range_, valueInputOption='RAW', insertDataOption='INSERT_ROWS', body={'values': job_detail_list}).execute()
    return "Job details added to sheet at row {}".format(result['updates']['updatedRange'].split('!')[1])

# Handle the '/start' command
@bot.message_handler(commands=['start'])
def start(message):
    bot.reply_to(message, "Hello! Please send me a LinkedIn job URL to check if you've applied before.")

# Handle incoming messages
@bot.message_handler(func=lambda message: True)
def handle_message(message):
    global job_detail_global
    global job_detail_url
    read_dataframe()
    # Extract the URL from the message
    url = message.text
    url_parsed = urlparse(url)
    if url_parsed.scheme != "http" and url_parsed.scheme != "https":
        bot.send_message(chat_id=message.chat.id, text="Please enter a valid URL.")
        return

    # Get the job details from the URL
    job_detail = get_linkedin_job_detail(url)
    if job_detail is None:
        bot.send_message(message.chat.id, "The job details could not be obtained from the URL")
        return "The job details could not be obtained from the URL"
        

    message_text = f"job_title: {job_detail['job_title']}\ncompany: {job_detail['company']}\nlocation: {job_detail['location']}"
    # send message to user
    bot.send_message(message.chat.id, message_text)

    job_detail_global = job_detail
    job_detail_url = url
    # Check if the job has been applied to before
    result = job_exists(job_detail, df)
    if result == "does not exist":
        # Ask the user if they want to add the job to the sheet
        markup = types.InlineKeyboardMarkup()
        accept_button = types.InlineKeyboardButton("Accept", callback_data="accept")
        decline_button = types.InlineKeyboardButton("Decline", callback_data="decline")
        markup.add(accept_button, decline_button)
        bot.reply_to(message, "Do you want to add this job to your applied sheet?", reply_markup=markup)
    else:
        bot.reply_to(message, result)

# Handle inline button callbacks
@bot.callback_query_handler(func=lambda call: True)
def handle_callback(call):
    if call.data == "accept":
        bot.edit_message_reply_markup(call.message.chat.id, call.message.message_id, reply_markup=InlineKeyboardMarkup([]))
        result =  result = write_to_sheet(job_detail_global, service, spreadsheet_id, range_,job_detail_url)
        bot.answer_callback_query(call.id, text=result)
        # Send message to user
        bot.send_message(call.message.chat.id, "Row added successfully!")
    elif call.data == "decline":
        bot.edit_message_reply_markup(call.message.chat.id, call.message.message_id, reply_markup=InlineKeyboardMarkup([]))
        bot.answer_callback_query(call.id, text="Job not added to sheet.")

# Start the bot
bot.polling()

